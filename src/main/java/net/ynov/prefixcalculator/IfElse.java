package net.ynov.prefixcalculator;

import java.io.IOException;
import java.io.StreamTokenizer;

import static net.ynov.prefixcalculator.Calculator.tokenizeToExpression;

public class IfElse
{
    private String booleanExpression;
    private String expressionIfTrue;
    private String expressionIfFalse;

    public IfElse(StreamTokenizer tokenizer) throws IOException
    {
        tokenizer.nextToken();
        if (tokenizer.ttype == '(') {
            this.booleanExpression = tokenizeToExpression(tokenizer);
            this.expressionIfTrue = tokenizeToExpression(tokenizer);
            this.expressionIfFalse = tokenizeToExpression(tokenizer);
        }

        tokenizer.nextToken();
        if (tokenizer.ttype != ')') {
            tokenizer.pushBack();
        }
    }

    public String getBooleanExpression()
    {
        return booleanExpression;
    }

    public String getExpressionIfTrue()
    {
        return expressionIfTrue;
    }

    public String getExpressionIfFalse()
    {
        return expressionIfFalse;
    }
}
