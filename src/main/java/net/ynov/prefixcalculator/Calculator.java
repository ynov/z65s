package net.ynov.prefixcalculator;

import java.io.*;
import java.util.*;

public class Calculator
{
    public final static boolean VERBOSE = true;
    public final static int LOOP_LIMIT = 100;

    public final static Map<String, StackItem> _VARS = new HashMap<>();
    public final static Map<String, StackItem> _FUNCS = new HashMap<>();

    private static Stack<StackItem> mainStack = new Stack<>();

    public static String tokenizeToExpression(StreamTokenizer tokenizer) throws IOException
    {
        int loop = 0;
        int j = 0;
        boolean end = false;

        StringBuilder sb = new StringBuilder();
        while (!end && tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            switch (tokenizer.ttype) {
                case '(':
                    if (j == 0) {
                        j++;
                    } else {
                        sb.append("( ");
                    }
                    break;

                case ')':
                    sb.append(") ");
                    j--;
                    if (j == 0) {
                        end = true;
                    }
                    break;

                case StreamTokenizer.TT_WORD:
                    sb.append(tokenizer.sval);
                    sb.append(" ");
                    break;

                case StreamTokenizer.TT_NUMBER:
                    sb.append(String.valueOf(tokenizer.nval));
                    sb.append(" ");
                    break;

                default:
                    sb.append(String.valueOf((char) tokenizer.ttype));
                    sb.append(" ");
                    break;
            }

            loop++;
            if (loop > LOOP_LIMIT) {
                break;
            }
        }
        return sb.toString();
    }

    public static void processExpression(String expression, Stack<StackItem> stack) throws IOException
    {
        int nNextOperand = 0;

        StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(expression));
        tokenizer.ordinaryChar('/');

        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            if (nNextOperand == 2 && stack.size() >= 3) {
                try {
                    processTop3StackItem(stack);
                } catch (Exception e) {
                    verboseLog(e.getMessage());
                }

                nNextOperand = 1;
            }

            switch (tokenizer.ttype) {
                case StreamTokenizer.TT_NUMBER:
                    stack.push(new StackItem(StackItem.NUMBER, tokenizer.nval));
                    nNextOperand++;
                    break;

                case StreamTokenizer.TT_WORD:
                    // boolean keywords
                    if (tokenizer.sval.equals("true")) {
                        stack.push(new StackItem(StackItem.BOOLEAN, true));
                    } else if (tokenizer.sval.equals("false")) {
                        stack.push(new StackItem(StackItem.BOOLEAN, false));
                    }

                    // function declaration
                    else if (tokenizer.sval.equals("func") && stack.isEmpty()) {
                        Function func = new Function(tokenizer);
                        _FUNCS.put(func.getName(), new StackItem(StackItem.FUNCTION, func));
                        stack.push(new StackItem(StackItem.MESSAGE,  "Function `" + func.getName() + "` defined."));
                    }

                    // ifelse
                    else if (tokenizer.sval.equals("ifelse")) {
                        IfElse ifelse = new IfElse(tokenizer);
                        Stack<StackItem> tmp = new Stack<>();
                        processExpression(ifelse.getBooleanExpression(), tmp);

                        if (finalizeStack(tmp).bval) {
                            tmp.clear();
                            processExpression(ifelse.getExpressionIfTrue(), tmp);
                            stack.push(finalizeStack(tmp));
                        } else {
                            tmp.clear();
                            processExpression(ifelse.getExpressionIfFalse(), tmp);
                            stack.push(finalizeStack(tmp));
                        }
                    }

                    // function call or variable
                    else {
                        String val = tokenizer.sval;
                        if (_FUNCS.containsKey(val)) {
                            Function func = _FUNCS.get(val).func;
                            processExpression(func.compileExpression(tokenizer), func.getStack());
                            stack.push(finalizeStack(func.getStack()));
                        } else {
                            stack.push(new StackItem(StackItem.VARIABLE, val));
                        }
                        nNextOperand++;
                    }
                    break;

                case '+':
                case '-':
                case '*':
                case '/':
                    stack.push(new StackItem(StackItem.A_OPERATOR, String.valueOf((char) tokenizer.ttype)));
                    nNextOperand = 0;
                    break;

                case '>':
                case '<':
                case '!':
                    String symbol = String.valueOf((char) tokenizer.ttype);

                    if (tokenizer.nextToken() == '=') {
                        stack.push(new StackItem(StackItem.B_OPERATOR, symbol + "="));
                    } else {
                        tokenizer.pushBack();
                        stack.push(new StackItem(StackItem.B_OPERATOR, symbol));
                    }
                    break;

                case '=':
                    if (tokenizer.nextToken() == '=') {
                        stack.push(new StackItem(StackItem.B_OPERATOR, "=="));
                    } else {
                        tokenizer.pushBack();
                        stack.push(new StackItem(StackItem.E_OPERATOR, "="));
                    }
                    break;

                case '(':
                case ')':
                    break;

                default:
                    System.out.println("Unknown operator: " + String.valueOf((char) tokenizer.ttype));
                    break;
            }
        }
    }

    private static void processTop3StackItem(Stack<StackItem> stack) throws Exception
    {
        StackItem bb = stack.pop();
        StackItem aa = stack.pop();
        StackItem ff = stack.pop();

        // Arithmetic
        if (ff.type == StackItem.A_OPERATOR) {
            double b = bb.type == StackItem.VARIABLE ? _VARS.get(bb.sval).nval : bb.nval;
            double a = aa.type == StackItem.VARIABLE ? _VARS.get(aa.sval).nval : aa.nval;

            switch (ff.sval) {
                case "+":
                    stack.push(new StackItem(StackItem.NUMBER, a + b));
                    break;
                case "-":
                    stack.push(new StackItem(StackItem.NUMBER, a - b));
                    break;
                case "*":
                    stack.push(new StackItem(StackItem.NUMBER, a * b));
                    break;
                case "/":
                    stack.push(new StackItem(StackItem.NUMBER, a / b));
                    break;
                default:
                    throw new Exception("-- Error code: A1 --");
            }
        }
        // Boolean
        else if (ff.type == StackItem.B_OPERATOR) {
            double b = bb.type == StackItem.VARIABLE ? _VARS.get(bb.sval).nval : bb.nval;
            double a = aa.type == StackItem.VARIABLE ? _VARS.get(aa.sval).nval : aa.nval;

            switch (ff.sval) {
                case ">":
                    stack.push(new StackItem(StackItem.BOOLEAN, a > b));
                    break;
                case ">=":
                    stack.push(new StackItem(StackItem.BOOLEAN, a >= b));
                    break;
                case "<":
                    stack.push(new StackItem(StackItem.BOOLEAN, a < b));
                    break;
                case "<=":
                    stack.push(new StackItem(StackItem.BOOLEAN, a <= b));
                    break;
                case "==":
                    stack.push(new StackItem(StackItem.BOOLEAN, a == b));
                    break;
                case "!=":
                    stack.push(new StackItem(StackItem.BOOLEAN, a != b));
                    break;
                default:
                    throw new Exception("-- Error code: A2 --");
            }
        }
        // Assignment
        else if (ff.type == StackItem.E_OPERATOR) {
            String variableName = aa.sval;
            _VARS.put(variableName, bb);

            stack.push(new StackItem(StackItem.MESSAGE, variableName + " <- " + bb.nval));
        }
        else {
            throw new Exception("-- Error code: B --");
        }
    }

    public static StackItem finalizeStack(Stack<StackItem> stack)
    {
        int loop = 0;

        while (!stack.isEmpty()) {
            if (stack.size() == 1) {
                return stack.pop();
            }

            else if (stack.size() >= 3) {
                try {
                    processTop3StackItem(stack);
                } catch (Exception e) {
                    stack.clear();
                    System.out.println(e.getMessage());
                }
            }

            else if (stack.size() == 2
                    && stack.lastElement().type == StackItem.BOOLEAN
                    && stack.firstElement().type == StackItem.B_OPERATOR) {
                boolean a = stack.pop().bval;
                String f = stack.pop().sval;

                if (f.equals("!")) {
                    stack.push(new StackItem(StackItem.BOOLEAN, !a));
                } else {
                    verboseLog("-- Error code: C --");
                    stack.clear();
                    return null;
                }
            }

            loop++;
            if (loop > LOOP_LIMIT) {
                verboseLog("-- Error code: D - Last stack size: " + stack.size() + " --");
                stack.clear();
                return null;
            }
        }

        verboseLog("-- Error code: E --");
        stack.clear();
        return null;
    }

    public static void outputLastItem(StackItem lastItem)
    {
        if (lastItem == null) {
            System.out.println("Invalid expression.");
        } else {
            if (lastItem.type == StackItem.NUMBER) {
                System.out.println(">> " + lastItem.nval);
            } else if (lastItem.type == StackItem.VARIABLE && _VARS.get(lastItem.sval) != null) {
                System.out.println(">> " + _VARS.get(lastItem.sval).nval);
            } else if (lastItem.type == StackItem.BOOLEAN) {
                System.out.println(">> " + lastItem.bval);
            } else if (lastItem.type == StackItem.MESSAGE) {
                System.out.println(">> " + lastItem.sval);
            } else {
                System.out.println("-- Undefined variable: " + lastItem.sval + " --");
                System.out.println("Invalid expression.");
            }
        }
        System.out.println();
    }

    public static void main(String[] args)
    {
        if (args.length < 1) {
            System.out.println("Usage: java -jar <<prefix-calculator.jar>> <<filename>> [> <<output>>]");
        } else {
            try {
                executeFile(args[0]);
            } catch (IOException e) {
                System.out.println("Error while reading file.");
                e.printStackTrace();
            }
        }
    }

    public static void executeFile(String filename) throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        boolean end = false;

        int i = 0;

        String line;
        while ((line = br.readLine()) != null && !end) {
            if (line.equals("quit")) {
                end = true;
            } else if (!line.equals("")) {
                System.out.printf("%d: %s\n", 1 + i++, line);
                processExpression(line, mainStack);
                outputLastItem(finalizeStack(mainStack));
            }
        }
    }

    public static void verboseLog(String message)
    {
        if (VERBOSE) {
            System.out.println(message);
        }
    }
}
