package net.ynov.prefixcalculator;

public class StackItem
{
    public static final int NUMBER     = 1;
    public static final int BOOLEAN    = 2;
    public static final int A_OPERATOR = 3;
    public static final int B_OPERATOR = 4;
    public static final int E_OPERATOR = 5;

    public static final int VARIABLE   = 6;
    public static final int FUNCTION   = 7;
    public static final int MESSAGE    = 8;

    public int type;
    public double nval = 0;
    public boolean bval = false;
    public String sval = "";
    public Function func = null;

    public StackItem(int type, double val)
    {
        this.type = type;
        this.nval = val;
    }

    public StackItem(int type, boolean val)
    {
        this.type = type;
        this.bval = val;
    }

    public StackItem(int type, String val)
    {
        this.type = type;
        this.sval = val;
    }

    public StackItem(int type, Function func)
    {
        this.type = type;
        this.func = func;
    }
}
