package net.ynov.prefixcalculator;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.*;

import static net.ynov.prefixcalculator.Calculator.*;

public class Function
{
    private String name;
    private String templateExpression;

    List<String> parameterList = new ArrayList<>();
    Map<String, StackItem> parameters = new HashMap<>();

    private Stack<StackItem> stack = new Stack<>();

    public Function(StreamTokenizer tokenizer) throws IOException
    {
        int loop = 0;
        int i = 0;
        int j = 0;

        StringBuilder sb = new StringBuilder();

        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            switch (tokenizer.ttype) {
                case StreamTokenizer.TT_WORD:
                    if (i == 0) {
                        this.name = tokenizer.sval;
                        i++;
                    }

                    if (j == 1) {
                        parameterList.add(":" + tokenizer.sval);
                    }

                    else if (j >= 3) {
                        if (!tokenizer.sval.equals("ifelse") &&
                            !tokenizer.sval.equals("true") &&
                            !tokenizer.sval.equals("false") &&
                            !tokenizer.sval.equals(this.name) &&
                            _FUNCS.get(tokenizer.sval) == null) {

                            sb.append(":");
                        }

                        sb.append(tokenizer.sval);
                        sb.append(" ");
                    }
                    break;

                case StreamTokenizer.TT_NUMBER:
                    if (j >= 3) {
                        sb.append(String.valueOf(tokenizer.nval));
                        sb.append(" ");
                    }
                    break;

                case '+':
                case '-':
                case '*':
                case '/':
                    sb.append(String.valueOf((char) tokenizer.ttype));
                    sb.append(" ");
                    break;

                case '<':
                case '>':
                case '!':
                case '=':
                    sb.append(String.valueOf((char) tokenizer.ttype));
                    if (tokenizer.nextToken() == '=') {
                        sb.append("=");
                    } else {
                        tokenizer.pushBack();
                    }
                    sb.append(" ");
                    break;

                case '(':
                case ')':
                    if (j >= 3) {
                        sb.append(String.valueOf((char) tokenizer.ttype));
                        sb.append(" ");
                    }
                    j++;
                    break;
            }

            loop++;
            if (loop > LOOP_LIMIT) {
                break;
            }
        }

        this.templateExpression = sb.toString();
    }

    public String compileExpression(StreamTokenizer tokenizer) throws IOException
    {
        String expression = templateExpression;

        boolean end = false;
        int loop = 0;
        int i = 0;
        int j = 0;

        while (!end) {
            tokenizer.nextToken();

            switch (tokenizer.ttype) {
                case '(':
                    if (j == 0) {
                        j++;
                    } else {
                        Stack<StackItem> tmp = new Stack<>();
                        processExpression(tokenizeToExpression(tokenizer), tmp);
                        StackItem si = finalizeStack(tmp);

                        parameters.put(parameterList.get(i), si);
                        i++;
                    }
                    break;
                case ')':
                    j--;
                    if (j == 0) {
                        end = true;
                    }
                    break;

                case StreamTokenizer.TT_NUMBER:
                    parameters.put(parameterList.get(i), new StackItem(StackItem.NUMBER, tokenizer.nval));
                    i++;
                    break;

                case StreamTokenizer.TT_WORD:
                    StackItem item = _VARS.get(tokenizer.sval);
                    if (item != null) {
                        parameters.put(parameterList.get(i), item);
                        i++;
                    }
                    break;
            }

            loop++;
            if (loop > LOOP_LIMIT) {
                break;
            }
        }

        for (String key : parameters.keySet()) {
            expression = expression.replace(key, String.valueOf(parameters.get(key).nval));
        }

        return expression;
    }

    public String getName()
    {
        return name;
    }

    public Stack<StackItem> getStack()
    {
        return stack;
    }

    public String getTemplateExpression()
    {
        return templateExpression;
    }
}
